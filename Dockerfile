FROM alpine:latest

MAINTAINER Carver <https://gitlab.com/carverhaines>

RUN apk add --no-cache rsync openssh

RUN mkdir -p /root/.ssh && \
    touch /root/.ssh/authorized_keys && \
    chmod go-rwx /root/.ssh/authorized_keys && \
    sed -i "s/#PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config && \
    sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin prohibit-password/g" /etc/ssh/sshd_config && \
    sed -i 's/root:!/root:*/' /etc/shadow

VOLUME /rsync
WORKDIR /rsync

ENTRYPOINT ["rsync"]
CMD ["--help"]
