# About
Allows you to use Rsync on any computer or server with docker installed, even if Rsync is not installed. Your Rsync command runs in the container, then container is destroyed.

Container tags reference version of rsync used, current version: *3.1.3*

## Usage as a client
#### Don't run using docker-compose...that is for the server
The below creates an alias that runs a one-time docker container that mounts your ssh keys to your home directory (change if your key directory is not home/user/.ssh/) and runs rsync command in the container mounting the current directory. Please note you have to be in a parent directory of both folders you want to sync. It will not work to rsync ../../other folder, since it does not know about that folder.

Add to your .bashrc
```
alias rsync='docker run -ti --rm -v $(pwd):/rsync -v $HOME/.ssh:/home/$USER/.ssh:ro  registry.gitlab.com/carverhaines/alpine-rsync:latest'
```
Example custom key location: `-v /path/to/keys:/home/$USER/.ssh:ro`

Then use rsync as you normally would
`rsync --version`

## Usage as a server

This will setup a persistent rsync server, with whatever directories you want to sync mounted to /rsync in the container. Then from the client you want to sync from just `rsync -Phav root@ip:/rsync localfolder`.

First copy the environment example file to .env, edit it with server directory you want to sync, the SSH port you want the container to run on, and the location of your server's authorized_keys file. Then run the
```
cp env.example .env
vim .env
docker-compose up -d
```

## Updating
This container repository will autoupdate with the newest version of Rsync when available, and version number above will be updated, so recommended adding a cron job or another alias to keep git updated, unless you want a specific version.
```
# Alias:
alias rsyncupdate='docker pull registry.gitlab.com/carverhaines/alpine-rsync:latest'
```
```
# Cron:
0 * * * *   docker pull registry.gitlab.com/carverhaines/alpine-rsync:latest
```
